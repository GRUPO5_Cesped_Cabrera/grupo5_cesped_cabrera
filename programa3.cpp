#include <iostream>
#include <ctime>
#include <cstdlib>
#include <bitset>

using std ::cout;
using std ::endl;
using std ::string;
using std ::bitset;


int * funcionRandom() 
{  
	static int r[256];
	int i;
	int *p;
	
	srand( (unsigned)time( NULL ));
	r[i] = rand();
}


int main()
{
	int *p;     
	int i;
	p = funcionRandom();     
    bool b;       
	bitset<8> bin(*p);     
	
	cout << "Numero entero random: " << *p << endl;
	cout << "Numero en binario: " << bin << endl;\
	
	for (i=7 ; i >= 0 ; i--)    
	{
		b = bin[i];
		if (b == 1)
		{
			cout << "El led del pin N " << 7-i << " esta ON" << endl;
		}else{
			cout << "El led del pin N " << 7-i << " esta OFF" << endl;
		}	
	}
	
	return 0;
}

