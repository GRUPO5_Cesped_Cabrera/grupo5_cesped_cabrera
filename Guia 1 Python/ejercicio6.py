"""Diseñe un programa que, dados dos números enteros, muestre por pantalla uno de estos
mensajes: <<El segundo es el cuadrado exacto del primero>>, <<El segundo es menor
que el cuadrado del primero>> o <<El segundo es mayor que el cuadrado del primero>>,
dependiendo de la verificación de la condición correspondiente al significado de cada
mensaje."""

a = input("Ingrese el primer numero entero: ")
b = input("Ingrese el segundo numero entero: ")

if a.isnumeric() and b.isnumeric():
    if int(a)**2 == int(b):
        print("El segundo es el cuadrado exacto del primero.")
    elif int(b) < int(a)**2:
        print("El segundo es menor que el cuadrado del primero.")
    elif int(b) > int(a)**2:
        print("El segundo es mayor que el cuadrado del primero.")
