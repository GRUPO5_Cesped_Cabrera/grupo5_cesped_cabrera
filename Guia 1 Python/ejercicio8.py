""""Asuma que posee dos variables, “varA” y “varB”, las cuales tienen valores asignados,
números o strings.
Escriba un código en Python que imprima por pantalla uno de los siguientes mensajes:
• “string involucrado”, si ambos varA y varB son strings.
• “mas grande”, si “varA” is mayor que “varB”.
• “igual”, si “varA”, es igual a “varB”.
• “mas pequeño”, si “varA” es menor que “varB” """

varA = input("Ingrese el valor de A: ")
varB = input("Ingrese el valor de b: ")

if varA.isalpha() and varB.isalpha():
    print("string involucrado")
elif varA > varB:
    print("string involucrado")
elif varA == varB:
    print("igual")
elif varA < varB:
    print("mas pequeño")
