"""Diseñe un programa que lea la edad de dos personas y diga quién es más joven, la
primera o la segunda. Tenga en cuenta que ambas pueden tener la misma edad. En tal
caso, debe desplegar un mensaje por pantalla adecuado."""

age1 = input("Ingrese la primera edad: ")
age2 = input("Ingrese la segunda edad: ")

if age1.isnumeric() and age2.isnumeric:
    if age1 == age2:
        print("Ambas personas tienen la misma edad...")

    if age1 > age2:
        print("Primera persona es mayor.")
    elif age1 < age2:
        print("Segunda persona es mayor.")

