#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int main()
{
	char contenido[] = "7EB3A50110140F395502560,7EB7A50110140F3957730188,7EB3A50110140F395502560,"
	                   "7EB7A50110140F3957730188,7EB3A50110140F395502560,7EB7A50110140F3957730188,"
					   "7EB3A50110140F395502560,7EB7A50110140F3957730188,7EB3A50110140F395502560,"
					   "7B7A50110140F3957730188,7EB3A50110140F395502560,7EB7A50110140F3957730188";
	char *token;
	char comparador[] = "7EB3A50110140F395502560";

	token = strtok(contenido, "," );
	
	if(token == NULL){
		printf("No hay datos.");
		return -1;
	}
	
	while(token != NULL ){
		if(strcmp(token, comparador) ==0){
			printf("Datos %s: TRAMA 1 - Temperatura ambiente\n", token);
		token= strtok(NULL, ",");
		}
		else{
			printf("Datos %s: TRAMA 2 - Humedad\n", token);
		token= strtok(NULL, ",");
		}	
	}
	return 0;
}
