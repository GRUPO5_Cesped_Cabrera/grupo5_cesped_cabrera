#include <iostream>
#include <time.h>
#include <stdlib.h>
#include <conio.h>
#include <ctime>

using namespace std;

int temperaturaPromedio(int arr[]);
int temperaturaMax( int arr[]);
int temperaturaMin(int arr[]);
int sumTEMP, numTEMP = 24;


int main(){
	

	
	srand(time(0));
	int TEMP1 = rand()%40;
	int TEMP2 = rand()%40;
	int TEMP3 = rand()%40;
	int TEMP4 = rand()%40;
	int TEMP5 = rand()%40;
	int TEMP6 = rand()%40;
	int TEMP7 = rand()%40;
	int TEMP8 = rand()%40;
	int TEMP9 = rand()%40;
	int TEMP10 = rand()%40;
	int TEMP11 = rand()%40;
	int TEMP12 = rand()%40;
	int TEMP13 = rand()%40;
	int TEMP14 = rand()%40;
	int TEMP15 = rand()%40;
	int TEMP16 = rand()%40;
	int TEMP17 = rand()%40;
	int TEMP18 = rand()%40;
	int TEMP19 = rand()%40;
	int TEMP20 = rand()%40;
	int TEMP21 = rand()%40;
	int TEMP22 = rand()%40;
	int TEMP23 = rand()%40;
	int TEMP24 = rand()%40;
	
	int Arr_TEMP[24] = {TEMP1, TEMP2, TEMP3, TEMP4, TEMP5, TEMP6, TEMP7, TEMP8, TEMP9, TEMP10, TEMP11,TEMP12, TEMP13, TEMP14, TEMP15, TEMP16, TEMP17, TEMP18, TEMP19, TEMP20, TEMP21, TEMP22, TEMP23, TEMP24};

	

    cout << "Temperatura promedio durante el dia: "<< temperaturaPromedio(Arr_TEMP) <<" grados a las " << rand()%24 << " Horas." << endl;
    cout << "Temperatura maxima del dia: " << temperaturaMax(Arr_TEMP)<< " grados a las " <<  rand()%24 <<" Horas." << endl;
    cout << "Temperatura minima del dia: " << temperaturaMin(Arr_TEMP)<< " grados a las " <<  rand()%24 <<" Horas." << endl;
    getch();
}

int temperaturaPromedio(int arr[]){

    for (int i = 0; i < numTEMP; i++){
        sumTEMP += arr[i];
    }
    return sumTEMP/numTEMP;
};

int temperaturaMax( int arr[]){
    int max = arr[0];

    for (int x = 1; x < numTEMP; x++){
        if(max < arr[x]){
            max = arr[x];
        }
    }
    return max;
};

int temperaturaMin(int arr[]){
	int min = arr[0];
	
	for (int y = 0; y < numTEMP; y++){
		
		if (min > arr[y]){
			min = arr[y];
		}	
	}
	return min;
};
