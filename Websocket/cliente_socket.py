import socket
import pandas as pd
import time
from cryptography.fernet import Fernet

HEADER = 64   #Ancho predeterminado 
PORT = 5050  #Puerto
FORMAT = 'utf-8'  #Formato
DISCONNECT_MESSAGE = "DESCONECTADO"  #String de estado
SERVER = "192.168.56.1"  #Ip
ADDR = (SERVER, PORT)  #Ip, puerto en variable ADDR

client = socket.socket(socket.AF_INET, socket.SOCK_STREAM) #Crear socket con ipv4 y dar la orden de stremear datos
client.connect(ADDR) #LInkear nuestro socket con variable cliente a la ip ADDR(IP, PUERTO)

def enviar(msg):  #Definir funcion send
    clave = open("clave.key", "rb").read()
    fernet = Fernet(clave)
    message = fernet.encrypt(msg.encode(FORMAT))
    print(message)
    msg_length = len(message)
    send_length = str(msg_length).encode(FORMAT)
    send_length += b' '*(HEADER-len(send_length))
    client.send(send_length) #Enviar largo del msg codificado
    client.send(message)  #Enviar msg
    print(client.recv(2048).decode(FORMAT)) #Imprimir mensaje del servidor codificado

def obtener():
    df = pd.read_csv("TotalesNacionales.csv")
    df_filtrado = df.dropna(0)   #Filtro los NAN por ceros
    for i in [0,1,2,3,4,5]:
        x= df_filtrado.iloc[i]
        #x = x.head(3)
        time.sleep(3)
        enviar(str(x))

while True:
    datamac = client.recv(2048).decode(FORMAT)
    print(datamac)
    data = input()
    client.send(data.encode(FORMAT))
    if data == "n":
        print("No se puedo verificar la MAC Address")
        print(client.recv(2048).decode(FORMAT))
        break
    else:
        #obtener()
        enviar("TEST")
        enviar(DISCONNECT_MESSAGE)
        break



