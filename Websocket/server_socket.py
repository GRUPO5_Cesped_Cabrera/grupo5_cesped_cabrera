import socket
import threading
import psutil, time
from cryptography.fernet import Fernet

HEADER = 64   #Ancho predeterminado
PORT = 5050   #Puerto
SERVER = socket.gethostbyname(socket.gethostname())   #IP
ADDR = (SERVER, PORT)   #IP, PUERTO en variable ADDR
FORMAT = 'utf-8'    #Formato de los mensajes
DISCONNECT_MESSAGE = "DESCONECTADO"   #String de estado

server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)    #Crear socket con ipv4 y dar la orden de stremear datos
server.bind(ADDR)   #Linkear nuestro socket con variable server a la ip ADDR(Ip, puerto)

def handle_client(conn, addr):  #Manejar multiples conexiones con argumento conn y addr
    print(f"[NUEVA CONEXION] {addr} se conecto.")  #Cada vez que se ejecute esta funcion imprime en pantalla la addres (addr)
    connected = True  #String de estado - mantiene conexionsend
    while connected:
        msg_length = conn.recv(HEADER).decode(FORMAT) #Largo del mensaje y lo codifica al formato utf-8
        if msg_length: 
            clave = open("clave.key", "rb").read()
            fernet = Fernet(clave)
            msg_length = int(msg_length) #Largo del mensaje se convierte a entero
            msg = fernet.decrypt(conn.recv(msg_length)).decode(FORMAT) #Guardamos el mensaje en variable msg
            print(f"[{addr}] {msg}") #Imprimir de donde viene el mensaje y el msg
            conn.send("[INFO] Mensaje recibido".encode(FORMAT)) #Enviar mensaje recibido decodificado en utf-8

            if msg == DISCONNECT_MESSAGE: #Si la variable msg es igual al estado "Desconectado", se corta la conexion
                connected = False #Cambia el estado de true a false y se corta el while infinito
    conn.close()    
def start():   #Funcion que escucha, recibe las conexiones y pasa esa conexion a la funcion handle_client
    server.listen()   #Escucha
    print(f"[ESPERANDO] El servidor esta esperando en {ADDR}")   #Mensaje de espera 
    while True:  #Ciclo infinito, consulta y espera conexiones
        conn, addr = server.accept() 
        conn.send("Para conectarte necesitas enviar tu MAC Address \nEscribe [Y] / [N] para aceptar".encode(FORMAT))
        data1 = conn.recv(HEADER).decode(FORMAT)
        if data1 == "y":
            loadbar()  #Barra de carga
            IP = list(obtenerMac(conn))
            print(f"MAC ADDRES:\n{IP}")
            time.sleep(1)
            print(f"[CONEXIONES ACTIVAS] = {threading.activeCount()}")  #Imprimir que existe una conexion activa, "cuandos hilos hay activos"
            time.sleep(3)
            thread = threading.Thread(target=handle_client, args=(conn, addr)) #Definir un thread que apunta a la funcion handle_client con los argumentos de la conexion aceptada
            thread.start()  #Iniciar thread
        else:
            print(f"[DESCONECTADO] {addr} se desconectado.")
            conn.send("[SERVIDOR] Desconectado por no enviar la MAC Address".encode(FORMAT))
            conn.close()
            
def obtenerMac(family):
    for interface, snics in psutil.net_if_addrs().items():
        for snic in snics:
            if snic.family == -1 :
                mac = snic.address
            if snic.family == 2 :
                yield (interface, snic.address, snic.netmask, mac)
def loadbar():
    total = 100
    archivo = "MAC Address"
    barra = 30
    for i in range(total+1):
        print('\r' + 'Verificando...: %s [%s%s]%.2f%%' % (archivo, '▮'*int(i*barra/total), '▨'*(barra-int(i*barra/total)),float(i/total*100)), end='')
        time.sleep(0.03)
    print("\n[INFO] MAC Address validada")  
print("[INICIANDO] El servidor se esta iniciando...")  #Imprimir que se esta "iniciando"
start()  #Ejecutar la funcion start

